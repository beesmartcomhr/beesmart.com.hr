$(document).ready(function () {


    $('#izdvoji').change(function () {
        var value = 0;
        var id_broj = $("#kosnicaId").val();
        if($(this).is(':checked')) {
            value = 1;
        }
        $.get("/api/v1/update_izdvoji/" + id_broj + "/" + value);
    });

    $('#sleep').change(function () {
        var value = 0;
        var id_broj = $("#kosnicaId").val();
        if($(this).is(':checked')) {
            value = 1;
        }
        $.get("/api/v1/update_sleep_mode/" + id_broj + "/" + value);
    });

    $('#animal').change(function () {
        var value = 0;
        var id_broj = $("#kosnicaId").val();
        if($(this).is(':checked')) {
            value = 1;
        }
        $.get("/api/v1/update_animal_mode/" + id_broj + "/" + value);
    });


});