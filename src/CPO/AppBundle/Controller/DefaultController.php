<?php

namespace CPO\AppBundle\Controller;

use CPO\AdminBundle\Entity\Kosnica;
use CPO\AdminBundle\Entity\Povijest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{


    /**
     * @Route("/", name="app_homepage")
     */
    public function indexAction()
    {
        return $this->render('@CPOApp/beesmart/index.html.twig');
    }

    /**
     * @Route("/api/v1/update_kosnica", name="app_update_kosnica", methods={"POST"})
     */
    public function kosnicaGetAction(Request $request)
    {
        $content = $request->getContent();
        if (!empty($content))
        {
            $params = json_decode($content, true);
        } else {
            return new JsonResponse(["Empty data"], 400);
        }
        $naslov = $params['naslov'];
        $temperatura = $params['temperatura'];
        $promjena = $params['promjena'];
        $temperatura_unutra = $params['temperatura_unutra'];
        $vlaga_unutra = $params['vlaga_unutra'];
        $vlaga = $params['vlaga'];
        $baterija = $params['baterija'];
        $tezina = $params['tezina'];
        $broj_pcela = $params['broj_pcela'];
        $vrijeme = $params['vrijeme'];
        $lokacija = $params['lokacija'];
        $identifikacijski_broj = $params['identifikacijski_broj'];
        
        $em = $this->getDoctrine()->getManager();
        $kosnica = $em->getRepository(Kosnica::class)->findOneBy(['identifikacijski_broj'=>$identifikacijski_broj]);
        $persist = false;
        if(!$kosnica) {
            $persist = true;
            $kosnica = new Kosnica();
        }
        if(empty($naslov)) {
            $naslov = "Košnica ID: " . $identifikacijski_broj;
        }
        if(!$persist) {
            $povijest = new Povijest();
            $povijest->setDatum($kosnica->getDatum());
            $povijest->setNaslov($kosnica->getNaslov());
            $povijest->setTemperatura($kosnica->getTemperatura());
            $povijest->setVlaga($kosnica->getVlaga());
            $povijest->setTezina($kosnica->getTezina());
            $povijest->setKosnica($kosnica);
            $povijest->setBrojPcela($kosnica->getBrojPcela());
            $povijest->setPromjena($kosnica->getPromjena());
            $povijest->setTemperaturaUnutra($kosnica->getTemperaturaUnutra());
            $povijest->setVlagaUnutra($kosnica->getVlagaUnutra());
            $povijest->setBaterija($kosnica->getBaterija());
            $povijest->setIdentifikacijskiBroj($kosnica->getIdentifikacijskiBroj());
            $povijest->setVrijeme($kosnica->getVrijeme());
            $povijest->setLokacija($kosnica->getLokacija());
            $povijest->setSleepMode($kosnica->getSleepMode());
            $povijest->setSustavZastiteZivotinja($kosnica->getSustavZastiteZivotinja());
            $povijest->setIzdvoji($kosnica->getIzdvoji());
            $em->persist($povijest);
            $em->flush();
        }
        $kosnica->setDatum(new \DateTime("now"));
        $kosnica->setNaslov($naslov);
        $kosnica->setTemperatura($temperatura);
        $kosnica->setVlaga($vlaga);
        $kosnica->setTezina($tezina);
        $kosnica->setBrojPcela($broj_pcela);
        $kosnica->setPromjena($promjena);
        $kosnica->setTemperaturaUnutra($temperatura_unutra);
        $kosnica->setVlagaUnutra($vlaga_unutra);
        $kosnica->setBaterija($baterija);
        $kosnica->setIdentifikacijskiBroj($identifikacijski_broj);

        $kosnica->setVrijeme($vrijeme);
        $kosnica->setLokacija($lokacija);
        $kosnica->setSleepMode(0);
        $kosnica->setSustavZastiteZivotinja(0);

        if($persist) {
            $kosnica->setIzdvoji(1);
            $em->persist($kosnica);
        }
        $em->flush();
        return new JsonResponse();
    }

    /**
     * @Route("/api/v1/update_sleep_mode/{id_broj}/{value}", name="app_sleep_mode", methods={"GET"})
     */
    public function updateSleepModeAction($id_broj,$value)
    {
        $em = $this->getDoctrine()->getManager();
        $kosnica = $em->getRepository(Kosnica::class)->findOneBy(['identifikacijski_broj'=>$id_broj]);
        if(!$kosnica) {
            return new JsonResponse(["Identifikacijski broj ne postoji"], 400);
        }
        $kosnica->setSleepMode($value);
        $em->flush();
        return new JsonResponse($kosnica->getSleepMode());
    }

    /**
     * @Route("/api/v1/update_animal_mode/{id_broj}/{value}", name="app_animal_mode", methods={"GET"})
     */
    public function updateAnimalModeAction($id_broj, $value)
    {
        $em = $this->getDoctrine()->getManager();
        $kosnica = $em->getRepository(Kosnica::class)->findOneBy(['identifikacijski_broj'=>$id_broj]);
        if(!$kosnica) {
            return new JsonResponse(["Identifikacijski broj ne postoji"], 400);
        }
        $kosnica->setSustavZastiteZivotinja($value);
        $em->flush();
        return new JsonResponse($kosnica->getSustavZastiteZivotinja());
    }

    /**
     * @Route("/api/v1/update_izdvoji/{id_broj}/{value}", name="app_izdvoji_update", methods={"GET"})
     */
    public function izdvojiAction($id_broj, $value)
    {
        $em = $this->getDoctrine()->getManager();
        $kosnica = $em->getRepository(Kosnica::class)->findOneBy(['identifikacijski_broj'=>$id_broj]);
        if(!$kosnica) {
            return new JsonResponse(["Identifikacijski broj ne postoji"], 400);
        }
        $kosnica->setIzdvoji($value);
        $em->flush();
        return new JsonResponse($kosnica->getIzdvoji());
    }

    /**
     * @Route("/api/v1/animal_mode_status/{id_broj}", name="app_animal_mode_status", methods={"GET"})
     */
    public function animalModeAction($id_broj)
    {
        $em = $this->getDoctrine()->getManager();
        $kosnica = $em->getRepository(Kosnica::class)->findOneBy(['identifikacijski_broj'=>$id_broj]);
        if(!$kosnica) {
            return new JsonResponse(["Identifikacijski broj ne postoji"], 400);
        }
        return new JsonResponse($kosnica->getSustavZastiteZivotinja());
    }

    /**
     * @Route("/api/v1/sleep_mode_status/{id_broj}", name="app_sleep_mode_status", methods={"GET"})
     */
    public function sleepModeAction($id_broj)
    {
        $em = $this->getDoctrine()->getManager();
        $kosnica = $em->getRepository(Kosnica::class)->findOneBy(['identifikacijski_broj'=>$id_broj]);
        if(!$kosnica) {
            return new JsonResponse(["Identifikacijski broj ne postoji"], 400);
        }
        return new JsonResponse($kosnica->getSleepMode());
    }

    /**
     * @Route("/api/v1/kosnica/{id_broj}", name="app_api_kosnica", methods={"GET"})
     */
    public function apiKosnicaAction($id_broj)
    {
        $em = $this->getDoctrine()->getManager();
        $kosnica = $em->getRepository(Kosnica::class)->findOneBy(['identifikacijski_broj'=>$id_broj]);
        if(!$kosnica) {
            return new JsonResponse(["Identifikacijski broj ne postoji"], 400);
        }
        return new JsonResponse([
            'identifikacijski_broj' => $kosnica->getIdentifikacijskiBroj(),
            'datum_zadnje_promjene' => $kosnica->getDatum()->format("d.m.Y h:i:s"),
            'temperatura' => $kosnica->getTemperatura(),
            'vlaga' => $kosnica->getVlaga(),
            'tezina' => $kosnica->getTezina(),
            'broj_pcela' => $kosnica->getBrojPcela(),
            'promjena' => $kosnica->getPromjena(),
            'temperatura_unutra' => $kosnica->getTemperaturaUnutra(),
            'vlaga_unutra' => $kosnica->getVlagaUnutra(),
            'baterija' => $kosnica->getBaterija(),
            'sleep_mode' => $kosnica->getSleepMode(),
            'sustav_zastite_zivotinja' => $kosnica->getSustavZastiteZivotinja(),
            'lokacija' => $kosnica->getLokacija(),
            'vrijeme' => $kosnica->getVrijeme()
        ]);
    }

    /**
     * @Route("/api/v1/povijest-mjerenja/{id_broj}", name="app_api_povijest_mjerenja", methods={"GET"})
     */
    public function apiPovijestAction($id_broj)
    {
        $kosnica = $this->getDoctrine()->getRepository(Kosnica::class)->findOneBy(['identifikacijski_broj'=> $id_broj]);
        $kosnice = $this->getDoctrine()->getRepository(Povijest::class)->findBy([
            'kosnica' => $kosnica
        ],[ 'id' => 'DESC']);
        if(!$kosnica) {
            return new JsonResponse(["Identifikacijski broj ne postoji"], 400);
        }
        $povijest = [];
        foreach ($kosnice as $item) {
            $arr = [
                'identifikacijski_broj' => $item->getIdentifikacijskiBroj(),
                'datum_azuriranja_kosnice' => $item->getDatum()->format("d.m.Y h:i:s"),
                'temperatura' => $item->getTemperatura(),
                'vlaga' => $item->getVlaga(),
                'tezina' => $item->getTezina(),
                'broj_pcela' => $item->getBrojPcela(),
                'promjena' => $item->getPromjena(),
                'temperatura_unutra' => $item->getTemperaturaUnutra(),
                'vlaga_unutra' => $item->getVlagaUnutra(),
                'baterija' => $item->getBaterija(),
                'sleep_mode' => $item->getSleepMode(),
                'sustav_zastite_zivotinja' => $item->getSustavZastiteZivotinja(),
                'lokacija' => $item->getLokacija(),
                'vrijeme' => $item->getVrijeme()
            ];
            array_push($povijest, $arr);
        }
        return new JsonResponse($povijest);
    }
}
