<?php

namespace CPO\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;


class LoginController extends Controller
{

    /**
     * @Route("/login", name="admin_login")
     */
    public function loginAction(Request $request)
    {
        if($this->getUser()) {
            return $this->redirectToRoute("admin_dashboard");
        }
        $authenticationUtils = $this->get('security.authentication_utils');
        $errors = $authenticationUtils->getLastAuthenticationError();
        $last_username = $authenticationUtils->getLastUsername();
        return $this->render('@CPOAdmin/Admin/login.html.twig', [
            'errors' => $errors,
            'username' => $last_username
        ]);
    }

    /**
     * @Route("/logout", name="admin_logout")
     */
    public function logoutAction()
    {
        //return $this->redirectToRoute("admin_login");
    }
}
