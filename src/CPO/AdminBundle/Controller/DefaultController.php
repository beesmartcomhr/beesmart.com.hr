<?php

namespace CPO\AdminBundle\Controller;

use CPO\AdminBundle\Entity\Kosnica;
use CPO\AdminBundle\Entity\Povijest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/admin/dashboard", name="admin_dashboard")
     */
    public function dashboardAction()
    {
        $kosnice = $this->getDoctrine()->getRepository(Kosnica::class)->findBy([
            'izdvoji' => 1
        ],[
            'id' => 'DESC'
        ], 3);

        return $this->render('@CPOAdmin/Admin/dashboard.html.twig', [
            'kosnice' => $kosnice,
        ]);
    }

    /**
     * @Route("/admin/beesmart", name="admin_about")
     */
    public function beeSmartActon()
    {
        return $this->render('@CPOAdmin/Admin/about.html.twig');
    }

    /**
     * @Route("/admin/kosnice", name="admin_kosnice")
     */
    public function kosniceAction()
    {
        $kosnice = $this->getDoctrine()->getRepository(Kosnica::class)->findBy([],[
            'id' => 'DESC'
        ], 3);

        return $this->render('@CPOAdmin/Admin/kosnice.html.twig', [
            'kosnice' => $kosnice,
        ]);
    }

    /**
     * @Route("/admin/povijest-mjerenja/{id}", name="admin_history")
     */
    public function povijestAction(Kosnica $kosnica)
    {
        $kosnice = $this->getDoctrine()->getRepository(Povijest::class)->findBy([
            'kosnica' => $kosnica
        ],[ 'id' => 'DESC']);

        return $this->render('@CPOAdmin/Admin/povijest_mjerenja.html.twig', [
            'kosnice' => $kosnice,
            'kosnica' => $kosnica,
        ]);
    }

    /**
     * @Route("/kosnica/{kosnica}", name="admin_kosnica")
     */
    public function kosnicaAction(Kosnica $kosnica)
    {
        $sleep_mode = $kosnica->getSleepMode();
        $animal_mode = $kosnica->getSustavZastiteZivotinja();
        $izdvoji = $kosnica->getIzdvoji();

        return $this->render('@CPOAdmin/Admin/kosnica.html.twig', [
            'kosnica' => $kosnica,
            'sleep_mode' => $sleep_mode,
            'animal_mode' => $animal_mode,
            'izdvoji' => $izdvoji,
        ]);
    }

    /**
     * @Route("/povijest-mjerenja/{kosnica}/{id}", name="admin_povijest_mjerenja")
     */
    public function povijestMjerenjaAction(Kosnica $kosnica, Povijest $povijest)
    {
        $sleep_mode = $povijest->getSleepMode();
        $animal_mode = $povijest->getSustavZastiteZivotinja();
        $izdvoji = $povijest->getIzdvoji();

        return $this->render('@CPOAdmin/Admin/povijest.html.twig', [
            'kosnica' => $povijest,
            'old_kosnica' => $kosnica,
            'sleep_mode' => $sleep_mode,
            'animal_mode' => $animal_mode,
            'izdvoji' => $izdvoji,
        ]);
    }
}
