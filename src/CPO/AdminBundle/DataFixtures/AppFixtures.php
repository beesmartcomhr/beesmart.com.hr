<?php
namespace CPO\AdminBundle\DataFixtures;

use CPO\AdminBundle\Entity\Admins;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private $encoder;

    public function load(ObjectManager $manager)
    {
        $admin = new Admins();
        $admin->setCreated(new \DateTime("now"));
        $admin->setAccess(new \DateTime("now"));
        $admin->setUsername("admin");
        $admin->setRole('ROLE_ADMIN');
        $admin->setStatus(1);
        $password = $this->encoder->encodePassword($admin, 'Admin12345!');
        $admin->setPassword($password);
        $manager->persist($admin);
        $manager->flush();
    }

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }
}