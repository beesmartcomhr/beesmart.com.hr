<?php

namespace CPO\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Povijest
 *
 * @ORM\Table(name="povijest")
 * @ORM\Entity(repositoryClass="CPO\AdminBundle\Repository\PovijestRepository")
 */
class Povijest
{

    /**
     * @ORM\ManyToOne(targetEntity="CPO\AdminBundle\Entity\Kosnica", inversedBy="povijest")
     * @ORM\JoinColumn(nullable=false)
     */
    private $kosnica;

    public function getKosnica(): Kosnica
    {
        return $this->kosnica;
    }

    public function setKosnica(Kosnica $kosnica)
    {
        $this->kosnica = $kosnica;

        return $this;
    }

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="naslov", type="string", length=255)
     */
    private $naslov;

    /**
     * @var integer
     *
     * @ORM\Column(name="identifikacijski_broj", type="integer", length=255, unique=false)
     */
    private $identifikacijski_broj;


    /**
     * @var integer
     *
     * @ORM\Column(name="izdvoji", type="integer", length=255)
     */
    private $izdvoji;

    /**
     * @var string
     *
     * @ORM\Column(name="temperatura", type="string", length=255)
     */
    private $temperatura;

    /**
     * @var string
     *
     * @ORM\Column(name="vlaga", type="string", length=255)
     */
    private $vlaga;

    /**
     * @var string
     *
     * @ORM\Column(name="promjena", type="string", length=255)
     */
    private $promjena;

    /**
     * @var string
     *
     * @ORM\Column(name="broj_pcela", type="string", length=255)
     */
    private $broj_pcela;

    /**
     * @var string
     *
     * @ORM\Column(name="tezina", type="string", length=255)
     */
    private $tezina;

    /**
     * @var string
     *
     * @ORM\Column(name="temperatura_unutra", type="string", length=255)
     */
    private $temperaturaUnutra;

    /**
     * @var string
     *
     * @ORM\Column(name="vlaga_unutra", type="string", length=255)
     */
    private $vlagaUnutra;

    /**
     * @var string
     *
     * @ORM\Column(name="baterija", type="string", length=255)
     */
    private $baterija;

    /**
     * @var string
     *
     * @ORM\Column(name="vrijeme", type="string", length=255, nullable=true)
     */
    private $vrijeme;

    /**
     * @var string
     *
     * @ORM\Column(name="lokacija", type="string", length=255, nullable=true)
     */
    private $lokacija;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datum", type="datetime")
     */
    private $datum;

    /**
     * @var int
     *
     * @ORM\Column(name="sleep_mode", type="integer")
     */
    private $sleepMode;

    /**
     * @var int
     *
     * @ORM\Column(name="sustav_zastite_zivotinja", type="integer")
     */
    private $sustavZastiteZivotinja;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set naslov
     *
     * @param string $naslov
     *
     * @return Povijest
     */
    public function setNaslov($naslov)
    {
        $this->naslov = $naslov;

        return $this;
    }

    /**
     * Get naslov
     *
     * @return string
     */
    public function getNaslov()
    {
        return $this->naslov;
    }

    /**
     * Set identifikacijski_broj
     *
     * @param integer $identifikacijski_broj
     *
     * @return Povijest
     */
    public function setIdentifikacijskiBroj($identifikacijski_broj)
    {
        $this->identifikacijski_broj = $identifikacijski_broj;

        return $this;
    }

    /**
     * Get identifikacijski_broj
     *
     * @return integer
     */
    public function getIdentifikacijskiBroj()
    {
        return $this->identifikacijski_broj;
    }

    /**
     * Set izdvoji
     *
     * @param integer $izdvoji
     *
     * @return Povijest
     */
    public function setIzdvoji($izdvoji)
    {
        $this->izdvoji = $izdvoji;

        return $this;
    }

    /**
     * Get izdvoji
     *
     * @return integer
     */
    public function getIzdvoji()
    {
        return $this->izdvoji;
    }

    /**
     * Set temperatura
     *
     * @param string $temperatura
     *
     * @return Povijest
     */
    public function setTemperatura($temperatura)
    {
        $this->temperatura = $temperatura;

        return $this;
    }

    /**
     * Get temperatura
     *
     * @return string
     */
    public function getTemperatura()
    {
        return $this->temperatura;
    }

    /**
     * Set vlaga
     *
     * @param string $vlaga
     *
     * @return Povijest
     */
    public function setVlaga($vlaga)
    {
        $this->vlaga = $vlaga;

        return $this;
    }

    /**
     * Get vlaga
     *
     * @return string
     */
    public function getVlaga()
    {
        return $this->vlaga;
    }

    /**
     * Set promjena
     *
     * @param string $promjena
     *
     * @return Povijest
     */
    public function setPromjena($promjena)
    {
        $this->promjena = $promjena;

        return $this;
    }

    /**
     * Get promjena
     *
     * @return string
     */
    public function getPromjena()
    {
        return $this->promjena;
    }

    /**
     * Set tezina
     *
     * @param string $tezina
     *
     * @return Povijest
     */
    public function setTezina($tezina)
    {
        $this->tezina = $tezina;

        return $this;
    }

    /**
     * Get tezina
     *
     * @return string
     */
    public function getTezina()
    {
        return $this->tezina;
    }

    /**
     * Set temperaturaUnutra
     *
     * @param string $temperaturaUnutra
     *
     * @return Povijest
     */
    public function setTemperaturaUnutra($temperaturaUnutra)
    {
        $this->temperaturaUnutra = $temperaturaUnutra;

        return $this;
    }

    /**
     * Get temperaturaUnutra
     *
     * @return string
     */
    public function getTemperaturaUnutra()
    {
        return $this->temperaturaUnutra;
    }

    /**
     * Set vlagaUnutra
     *
     * @param string $vlagaUnutra
     *
     * @return Povijest
     */
    public function setVlagaUnutra($vlagaUnutra)
    {
        $this->vlagaUnutra = $vlagaUnutra;

        return $this;
    }

    /**
     * Get vlagaUnutra
     *
     * @return string
     */
    public function getVlagaUnutra()
    {
        return $this->vlagaUnutra;
    }

    /**
     * Set baterija
     *
     * @param string $baterija
     *
     * @return Povijest
     */
    public function setBaterija($baterija)
    {
        $this->baterija = $baterija;

        return $this;
    }

    /**
     * Get baterija
     *
     * @return string
     */
    public function getBaterija()
    {
        return $this->baterija;
    }

    /**
     * Set vrijeme
     *
     * @param string $vrijeme
     *
     * @return Povijest
     */
    public function setVrijeme($vrijeme)
    {
        $this->vrijeme = $vrijeme;

        return $this;
    }

    /**
     * Get vrijeme
     *
     * @return string
     */
    public function getVrijeme()
    {
        return $this->vrijeme;
    }

    /**
     * Set lokacija
     *
     * @param string $lokacija
     *
     * @return Povijest
     */
    public function setLokacija($lokacija)
    {
        $this->lokacija = $lokacija;

        return $this;
    }

    /**
     * Get lokacija
     *
     * @return string
     */
    public function getLokacija()
    {
        return $this->lokacija;
    }

    /**
     * Set $broj_pcela
     *
     * @param string $broj_pcela
     *
     * @return Povijest
     */
    public function setBrojPcela($broj_pcela)
    {
        $this->broj_pcela = $broj_pcela;

        return $this;
    }

    /**
     * Get $broj_pcela
     *
     * @return string
     */
    public function getBrojPcela()
    {
        return $this->broj_pcela;
    }

    /**
     * Set datum
     *
     * @param \DateTime $datum
     *
     * @return Povijest
     */
    public function setDatum($datum)
    {
        $this->datum = $datum;

        return $this;
    }

    /**
     * Get datum
     *
     * @return \DateTime
     */
    public function getDatum()
    {
        return $this->datum;
    }

    /**
     * Set sleepMode
     *
     * @param integer $sleepMode
     *
     * @return Povijest
     */
    public function setSleepMode($sleepMode)
    {
        $this->sleepMode = $sleepMode;

        return $this;
    }

    /**
     * Get sleepMode
     *
     * @return int
     */
    public function getSleepMode()
    {
        return $this->sleepMode;
    }

    /**
     * Set sustavZastiteZivotinja
     *
     * @param integer $sustavZastiteZivotinja
     *
     * @return Povijest
     */
    public function setSustavZastiteZivotinja($sustavZastiteZivotinja)
    {
        $this->sustavZastiteZivotinja = $sustavZastiteZivotinja;

        return $this;
    }

    /**
     * Get sustavZastiteZivotinja
     *
     * @return int
     */
    public function getSustavZastiteZivotinja()
    {
        return $this->sustavZastiteZivotinja;
    }
}

